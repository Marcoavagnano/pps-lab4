package u04lab.polyglot.a01a

import Logics.*
import u04lab.polyglot.Pair
import scala.util.Random

/** solution and descriptions at https://bitbucket.org/mviroli/oop2019-esami/src/master/a01a/sol2/ */

import u04lab.code.List

class LogicsImpl(private val size: Int, private val boat: Int) extends Logics :

  private var hit: List[Pair[Int, Int]] = List.Nil()
  private val FAILURES = 5
  val random: Random = Random()
  val randomWithSeed: Random = Random(42)
  private val boatRow = random.nextInt(size)
  private val boatLeftCol = size - (boat + 1)
  private val boatSize = boat
  private var failures = 0

  def hit(row: Int, col: Int) =
    if row == boatRow && col >= boatLeftCol && col < boatLeftCol + boatSize then
      hit = List.append(List.Cons(Pair(row, col), List.Nil()), hit)
      if List.length(hit) == boatSize then Result.WON else Result.HIT
    else
      failures = failures + 1
      if failures == FAILURES then Result.LOST else Result.MISS




