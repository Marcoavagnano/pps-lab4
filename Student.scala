package u04lab.code

import List.*
import List.*

trait Student:
  def name: String

  def year: Int

  def enrolling(course: Course*): Unit // the student participates to a Course

  def courses: List[String] // names of course the student participates to

  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?

trait Course:
  def name: String

  def teacher: String

object Student:
  def apply(name: String, year: Int = 2017): Student = StudentImpl(name, year)

object Course:
  def apply(name: String, teacher: String): Course = CourseImpl(name, teacher)


case class CourseImpl(override val name: String, override val teacher: String) extends Course

case class StudentImpl(override val name: String, override val year: Int
                      ) extends Student :
  override def enrolling(courses: Course*): Unit =
    courses.foreach(c => lCourses = append(Cons(c, Nil()), lCourses))

  override def courses: List[String] = map(lCourses)(_.name)

  override def hasTeacher(teacher: String): Boolean = contains(map(lCourses)(_.teacher), teacher)

  private var lCourses: List[Course] = Nil()

object FactoryList:
  def apply[E](elem: E*): List[E] =
    var lst: List[E] = List.Nil()
    elem.foreach(e => lst = List.append(List.Cons(e, List.Nil()), lst))
    lst

  val cPPS = Course("PPS", "Viroli")
  val cPCD = Course("PCD", "Viroli")
  val cSDR = Course("SDR", "Viroli")
  val courses = FactoryList(cPPS, cPCD, cSDR)

object SameTeacher:
  def unapply(list: List[Course]): Option[String] = list match
    case Cons(h, t) if length(list) == length(filter(list)(c => c.teacher == h.teacher)) => Option.Some(h.teacher)
    case _ => Option.None()

@main def checkStudents(): Unit =
  val cPPS = Course("PPS", "Viroli")
  val cPCD = Course("PCD", "Viroli")
  val cSDR = Course("SDR", "Viroli")
  val s1 = Student("mario", 2015)
  val s2 = Student("gino", 2016)
  val s3 = Student("rino") // defaults to 2017
  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  val courses: List[Course] = FactoryList(cPPS, cPCD, cSDR)
  
  //NON HO CAPITO BENE COME ESEGUIRE IL MATCH TRA LA LISTA E sameTeacher
  courses match
    case sameTeacher(t) => println(s"$courses have sameteacher $t")
    case _ => println (s"$courses have different teachers ")
  println(
    (s1.courses, s2.courses, s3.courses)
  ) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher("Ricci")) // true

/** Hints:
 *   - simply implement Course, e.g. with a case class
 *   - implement Student with a StudentImpl keeping a private Set of courses
 *   - try to implement in StudentImpl method courses with map
 *   - try to implement in StudentImpl method hasTeacher with map and find
 *   - check that the two println above work correctly
 *   - refactor the code so that method enrolling accepts a variable argument Course*
 */
